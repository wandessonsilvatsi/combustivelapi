package com.indra.desafio.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indra.desafio.model.Usuario;
import com.indra.desafio.model.dto.UsuarioDTO;
import com.indra.desafio.model.dto.UsuarioResponseDTO;
import com.indra.desafio.services.UsuarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/usuarios")
@Api(value="API REST para administração dos Usuários")
@CrossOrigin(origins="*")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@ApiOperation(value="Retorna a lista de todos os usuários")
	@GetMapping
	public ResponseEntity<?> getAll() {
		List<Usuario> usuarios = this.usuarioService.findAll();
		List<UsuarioResponseDTO> usuariosDTO = new ArrayList<UsuarioResponseDTO>();
		
		for (Usuario usuario : usuarios) {
			UsuarioResponseDTO usuarioDTO = UsuarioResponseDTO.converteParaDTO(usuario);
			usuariosDTO.add(usuarioDTO);
		}
		
		return new ResponseEntity<>(usuariosDTO, HttpStatus.OK);
	}
	
	@ApiOperation(value="Retorna um único usuário a partir do seu identificador")
	@GetMapping("/{id}")
	public ResponseEntity<?> listaPorId(@PathVariable(value="id") long id) {
		Optional<Usuario> usuario = this.usuarioService.findById(id);
		
		if (usuario.isPresent()) {
			return new ResponseEntity<>(UsuarioResponseDTO.converteParaDTO(usuario.get()), HttpStatus.OK);
		}
		
		return new ResponseEntity<>("Usuário não encontrado", HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value="Cadastra um novo usuário")
	@PostMapping
	public ResponseEntity<?> salvar(@RequestBody UsuarioDTO usuarioDTO) {
		Usuario usuario = this.usuarioService.save(usuarioDTO.converteParaUsuario());
		return new ResponseEntity<>(UsuarioResponseDTO.converteParaDTO(usuario), HttpStatus.CREATED);
	}
	
	@ApiOperation(value="Atualiza um usuário já existente")
	@PutMapping
	public ResponseEntity<?> atualizar(@RequestBody Usuario usuarioNovo) {
		Optional<Usuario> user = this.usuarioService.findById(usuarioNovo.getId());
		
		if (user.isPresent()) {
			this.usuarioService.update(user.get().getId(), usuarioNovo);
			return new ResponseEntity<>(UsuarioResponseDTO.converteParaDTO(usuarioNovo),HttpStatus.OK);
		}
		
		return new ResponseEntity<>("Id do usuário não encontrado.", HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value="Remove um usuário")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remover(@PathVariable(value="id") long id){
		Optional<Usuario> usuario = this.usuarioService.findById(id);
		
		if (usuario.isPresent()) {
			this.usuarioService.delete(usuario.get().getId());
			return new ResponseEntity<>("Usuário removido com sucesso!", HttpStatus.OK);
		}
		
		return new ResponseEntity<>("Id do usuário não encontrado", HttpStatus.NOT_FOUND);
	}
}
