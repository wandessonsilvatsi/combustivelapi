package com.indra.desafio.model.dto;

import com.indra.desafio.model.Usuario;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UsuarioResponseDTO {
	private Long id;
	private String nome;
	private String email;
	private String login;
	private boolean admin;
	
	public static UsuarioResponseDTO converteParaDTO(Usuario usuario) {
	    return new UsuarioResponseDTO(
	    		usuario.getId(), 
	    		usuario.getNome(), 
	    		usuario.getEmail(),
	    		usuario.getLogin(),
	    		usuario.isAdmin());
	}
}
