package com.indra.desafio.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Arquivo {
	private String nome;
	private String path;
}
