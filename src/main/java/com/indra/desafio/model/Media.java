package com.indra.desafio.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Media implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String atributo;
	private double mediaValorCompra;
	private double mediaValorVenda;


}
