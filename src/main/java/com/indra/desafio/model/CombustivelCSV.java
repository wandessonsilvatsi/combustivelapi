package com.indra.desafio.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CombustivelCSV implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String siglaRegiao;
	private String siglaEstado;	
	private String municipio;	
	private String revendedora;
	private Integer codInstalacao;
	private String produto;
	private Date dataColeta;
	private Double valorCompra;
	private Double valorVenda;
	private String unidadeMedida;
	private String bandeira;

}
