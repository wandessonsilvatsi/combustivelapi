package com.indra.desafio.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indra.desafio.model.Arquivo;
import com.indra.desafio.model.CombustivelCSV;
import com.indra.desafio.model.Media;
import com.indra.desafio.repository.CombustivelCSVRepository;

@Service
public class CombustivelCSVService {
	
	@Autowired
	private CombustivelCSVRepository repository;
	
	public void lerArquivoCSV(Arquivo arquivo) {
		String path = arquivo.getPath() + "/" + arquivo.getNome();
		
		File pasta = null;
		try {
			pasta = new File(CombustivelCSVService.class.getResource(".").toURI());
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} 
		String dir = pasta.getAbsolutePath();
		String[] pathArq = dir.split("angular");
		String pathArquivo = pathArq[0] + "angular\\desafio\\" + arquivo.getNome();
		String p = pathArquivo.replace("\\", "/");
		try(BufferedReader br = new BufferedReader( new InputStreamReader(new FileInputStream(p), "UTF-8") )){
			String line = br.readLine();
			int numLine = 1;
			while(line != null) {
				System.out.println(line);
				if (numLine > 1)
					carregaBaseDeDados(line);
				line = br.readLine();
				numLine++;
			}
		} catch(IOException e) {
			System.out.println("Error " + e.getMessage());
		}
	}
	
	public void carregaBaseDeDados(String line) {
		String [] contentCSV = line.split("  ");
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			if(contentCSV[7].equals(""))
				contentCSV[7] = "0";
			else
				contentCSV[7] = contentCSV[7].replace(',', '.');
			
			if(contentCSV[8].equals(""))
				contentCSV[8] = "0";
			else
				contentCSV[8] = contentCSV[8].replace(',', '.');
			
			
			CombustivelCSV obj = new CombustivelCSV(null, contentCSV[0], contentCSV[1], contentCSV[2], contentCSV[3], 
					Integer.parseInt(contentCSV[4]), contentCSV[5], sdf.parse(contentCSV[6]), Double.parseDouble(contentCSV[7]), 
					Double.parseDouble(contentCSV[8]), contentCSV[9], contentCSV[10]);
			
			repository.save(obj);

			
		}catch(ParseException e) {
			System.out.println("Error " + e.getMessage());
		} catch(NumberFormatException e) {
			System.out.println("Error " + e.getMessage());
		}
		
	}

	public List<String> getMunicipios() {
		return repository.getMunicipios();
	}

	public float mediaDePrecoPorMunicipio(String municipio) {
		return repository.mediaDePrecoPorMunicipio(municipio);		
	}

	public List<String> getSiglasRegiao() {
		return repository.getSiglasRegiao();
	}

	public List<CombustivelCSV> getDadosPorSigla(String sigla) {
		return repository.getDadosPorSigla(sigla);
	}
	
	public List<CombustivelCSV> getDadosAgrupadosPorDistribuidora() {
		return repository.getDadosAgrupadosPorDistribuidora();
	}
	
	public List<CombustivelCSV> getDadosAgrupadosPorDataColeta() {
		return repository.getDadosAgrupadosPorDataColeta();
	}
	
	public List<Media> valorMedioCompraVendaBandeira() {
		return repository.valorMedioCompraVendaBandeira();
	}
	
	public List<Media> valorMedioCompraVendaMunicipio() {
		return repository.valorMedioCompraVendaMunicipio();
	}

}
