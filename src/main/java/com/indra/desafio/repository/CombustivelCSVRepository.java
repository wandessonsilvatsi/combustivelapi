package com.indra.desafio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.indra.desafio.model.CombustivelCSV;
import com.indra.desafio.model.Media;

public interface CombustivelCSVRepository extends JpaRepository<CombustivelCSV, Long> {
	@Query("select distinct c.municipio from CombustivelCSV c")
	List<String> getMunicipios();

	@Query("select avg(c.valorVenda) from CombustivelCSV c where c.municipio = ?1")
	float mediaDePrecoPorMunicipio(String nomeMunicipio);

	@Query("select distinct c.siglaRegiao from CombustivelCSV c")
	List<String> getSiglasRegiao();

	@Query("select c from CombustivelCSV c where c.siglaRegiao = ?1")
	List<CombustivelCSV> getDadosPorSigla(String sigla);

	@Query("select c from CombustivelCSV c order by c.revendedora")
	List<CombustivelCSV> getDadosAgrupadosPorDistribuidora();

	@Query("select c from CombustivelCSV c order by c.dataColeta")
	List<CombustivelCSV> getDadosAgrupadosPorDataColeta();
	
	@Query("select new com.indra.desafio.model.Media( c.bandeira, avg(c.valorCompra), avg(c.valorVenda) ) from CombustivelCSV c group by c.bandeira")
	List<Media> valorMedioCompraVendaBandeira();

	@Query("select new com.indra.desafio.model.Media( c.municipio, avg(c.valorCompra), avg(c.valorVenda) ) from CombustivelCSV c group by c.municipio")
	List<Media> valorMedioCompraVendaMunicipio();

}
