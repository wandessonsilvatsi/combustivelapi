package com.indra.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indra.desafio.model.Historico;

public interface HistoricoRepository extends JpaRepository<Historico, Long>{

}
