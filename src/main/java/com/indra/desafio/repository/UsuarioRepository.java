package com.indra.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indra.desafio.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

}
